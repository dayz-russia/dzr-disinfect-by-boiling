class CfgPatches
{
	class dzr_disinfect_by_boiling
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_disinfect_by_boiling
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "dzr_disinfect_by_boiling";
		dir = "dzr_disinfect_by_boiling";
		name = "dzr_disinfect_by_boiling";
		inputs = "dzr_disinfect_by_boiling/Data/Inputs.xml";
		dependencies[] = {"Game", "World", "Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_disinfect_by_boiling/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_disinfect_by_boiling/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_disinfect_by_boiling/5_Mission"};
			};

		};
	};
};